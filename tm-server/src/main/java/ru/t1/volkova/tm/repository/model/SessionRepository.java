package ru.t1.volkova.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.volkova.tm.api.repository.model.ISessionRepository;
import ru.t1.volkova.tm.comparator.CreatedComparator;
import ru.t1.volkova.tm.comparator.NameComparator;
import ru.t1.volkova.tm.comparator.StatusComparator;
import ru.t1.volkova.tm.model.Session;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Override
    public @Nullable List<Session> findAll(@Nullable final String userId) {
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable List<Session> findAll(
            @Nullable final String userId,
            @NotNull final Comparator comparator
    ) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId " +
                        "ORDER BY e." + param, Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public @Nullable Session findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        return entityManager
                .createQuery("SELECT e FROM Session e WHERE e.user.id = :userId " +
                        "AND e.id = :id", Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultList().stream()
                .findFirst().orElse(null);
    }

    @Override
    public Session findOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        return entityManager
                .createQuery("SELECT e FROM Session e WHERE e.user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList().stream()
                .skip(index).findFirst()
                .orElse(null);
    }

    @Override
    public int getSize(@Nullable final String userId) {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Session e WHERE e.user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

    @Override
    public void removeOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void removeOneByIndex(
            @Nullable final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final Session session = findOneByIndex(userId, index);
        if (session == null) {
            return;
        }
        entityManager.remove(findOneById(userId, session.getId()));
    }

    @Override
    public void clear(@Nullable final String userId) {
        @Nullable final List<Session> sessions = findAll(userId);
        if (sessions == null) return;
        for (@NotNull final Session session : sessions) {
            removeOneById(userId, session.getId());
        }
    }

    @Override
    public @NotNull List<Session> findAll() {
        return entityManager.createQuery("FROM Session", Session.class).getResultList();
    }

    @Override
    public @Nullable List<Session> findAll(@NotNull final Comparator comparator) {
        @Nullable String param;
        if (comparator == CreatedComparator.INSTANSE) param = "created";
        else if (comparator == StatusComparator.INSTANSE) param = "status";
        else if (comparator == NameComparator.INSTANSE) param = "name";
        else param = null;
        return entityManager.createQuery("SELECT e FROM Session e WHERE e.user.id = :userId " +
                        "ORDER BY e." + param, Session.class)
                .getResultList();
    }

    @Override
    public void clear() {
        @NotNull final List<Session> sessions = findAll();
        for (@NotNull final Session session : sessions) {
            removeOneById(session.getId());
        }
    }

    @Override
    public @NotNull Session findOneById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Override
    public @Nullable Session findOneByIndex(@NotNull final Integer index) {
        return entityManager
                .createQuery("FROM Session", Session.class)
                .setFirstResult(index).setMaxResults(1).getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        @Nullable final Session session = findOneByIndex(index);
        if (session == null) {
            return;
        }
        removeOneById(session.getId());
    }

    @Override
    public int getSize() {
        return entityManager
                .createQuery("SELECT COUNT(e) FROM Session e", Long.class)
                .setMaxResults(1)
                .getSingleResult()
                .intValue();
    }

}
