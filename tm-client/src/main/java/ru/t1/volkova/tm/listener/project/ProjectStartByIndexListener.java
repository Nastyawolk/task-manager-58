package ru.t1.volkova.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.volkova.tm.dto.request.project.ProjectChangeStatusByIndexRequest;
import ru.t1.volkova.tm.enumerated.Status;
import ru.t1.volkova.tm.event.ConsoleEvent;
import ru.t1.volkova.tm.util.TerminalUtil;

import java.sql.SQLException;

@Component
public final class ProjectStartByIndexListener extends AbstractProjectListener {

    @NotNull
    private static final String DESCRIPTION = "Start project by index.";

    @NotNull
    private static final String NAME = "project-start-by-index";

    @Override
    @EventListener(condition = "@projectStartByIndexListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws SQLException {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final ProjectChangeStatusByIndexRequest request = new ProjectChangeStatusByIndexRequest(getToken());
        request.setIndex(index);
        request.setStatus(Status.IN_PROGRESS);
        getProjectEndpoint().changeProjectStatusByIndex(request);
    }

    @Override
    public @NotNull String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public @NotNull String getName() {
        return NAME;
    }

}
