package ru.t1.volkova.tm.dto.response.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
@Getter
@Setter
public final class DropLiquibaseSchemeResponse extends AbstractResultResponse {

    public DropLiquibaseSchemeResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}
