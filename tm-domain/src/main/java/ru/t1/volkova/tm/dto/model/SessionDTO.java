package ru.t1.volkova.tm.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Cache;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.volkova.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "tm_session")
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public final class SessionDTO extends AbstractUserOwnedDTOModel {

    @NotNull
    @Column(nullable = false)
    private Date date = new Date();

    @Nullable
    @Enumerated(EnumType.STRING)
    private Role role = null;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SessionDTO session = (SessionDTO) o;
        return session.getId().equals(((SessionDTO) o).getId()) && session.getUserId().equals(((SessionDTO) o).getUserId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate(), getRole());
    }

}
